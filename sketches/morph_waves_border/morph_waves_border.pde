import java.util.*;
import typomorph.*;
import geomerative.*;

MorphWaves morph_waves;
MorphWaves morph_waves_2;

String[] letters = {"N", "e", "x", "t", "!"};
int cpt = 0;
int n_frames = 40;

boolean record = false;

ArrayList<TypoWaves> waves;
int cur_wave = 0;

float ease(float p, float g) {
  if (p < 0.5)
    return 0.5 * pow(2*p, g);
  else
    return 1 - 0.5 * pow(2*(1 - p), g);
}

void setup() {
  size(1000, 1000, FX2D);
  smooth();

  RG.setPolygonizer(RG.UNIFORMSTEP);
  RG.init(this);

  Typomorph.init(this);
  Typomorph.setDefaultFont("FreSans.ttf", 900);
  Typomorph.setDefaultSampling(200, Typomorph.UNIFORM);

  waves = new ArrayList<TypoWaves>();

  for (int i = 0; i < letters.length; i++){
    Typo typo = Typomorph.createTypo(letters[i], -width/2, height/2);
    waves.add(new TypoWaves(typo, 400, 0, 2, 2));
    typo = Typomorph.createTypo(letters[i], width/2, height/2);
    waves.add(new TypoWaves(typo, 100, 0, 2, 2));
    typo = Typomorph.createTypo(letters[i], 3*width/2, height/2);
    waves.add(new TypoWaves(typo, 400, 0, 2, 2));
  }


  morph_waves = new MorphWaves(waves.get(1), waves.get(3));
  morph_waves_2 = new MorphWaves(waves.get(2), waves.get(4));
}

void draw(){
  background(0);
  noFill();
  stroke(255);
  strokeWeight(2);

  int i_frame = cpt%n_frames;

  float pos = ease(float(i_frame)/n_frames, 2);
  float w = map(abs(pos - 0.5), 0, 0.5, 0.6, 0.01);

  morph_waves.bezierMorph(pos, w);
  morph_waves_2.bezierMorph(pos, w);
  cpt +=1;

  if (cpt % n_frames == 0){
    cur_wave = (cur_wave + 1) % letters.length;
    int cur_wave_next = (cur_wave + 1)%letters.length;
    morph_waves = new MorphWaves(waves.get(3*cur_wave+1), waves.get(3*cur_wave_next));
    morph_waves_2 = new MorphWaves(waves.get(3*cur_wave+2), waves.get(3*cur_wave_next + 1));
  }

  if (record){
    save("screen-" + nf(cpt, 4));
  }

  if (cpt >= n_frames * letters.length * 2){
    record = false;
  }
}

void keyPressed(){
  if (key == 'R'){
    record = true;
    cpt = 0;
  }
}
