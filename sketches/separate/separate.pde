
import java.util.*;
import typomorph.*; 
import geomerative.*;

TypoWaves inside_waves;
TypoWaves outside_waves;

PGraphics pg;

void setup() {
  size(1000, 1000, FX2D);

  pg = createGraphics(1000, 1000);

  RG.setPolygonizer(RG.UNIFORMSTEP);
  RG.init(this);

  Typomorph.init(this);
  Typomorph.setDefaultFont("FreeSans.ttf", 900);


  Typo typo = Typomorph.createTypo("C", new RPoint(height/2, width/2));
  typo.resample(2000, Typomorph.UNIFORM);

  Typo typo_int = typo.separate();

  outside_waves = new TypoWaves(typo, 40, 0, 4, 1);
  inside_waves = new TypoWaves(typo_int, 40, -20, 1, -1.5);
  pg.beginDraw();
  pg.strokeWeight(2);
  pg.endDraw();
}

void draw() {
  background(120*(sin(PI/2 + millis()/1000.0)+1));

  outside_waves.update();
  inside_waves.update();

  pg.beginDraw();
  pg.stroke(128*(sin(millis()/1000.0)+1));
  outside_waves.drawOnGraphics(pg);
  // inside_waves.drawOnGraphics(pg);
  pg.endDraw();
  image(pg, 0, 0);
}
