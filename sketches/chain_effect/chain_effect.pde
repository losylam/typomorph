import java.util.*;
import typomorph.*; 
import geomerative.*;

Typomorph TM;

Typo typo;


void setup() {
  size(1000, 1000, FX2D);
  smooth();

  RG.setPolygonizer(RG.UNIFORMSTEP);
  RG.setPolygonizerStep(1);
  RG.init(this);

  TM = new Typomorph(this);
  TM.setDefaultFont("FreeSans.ttf", 800);
  typo = TM.createTypo("G", new RPoint(height/2, width/2));
  typo.resample(800, TM.UNIFORM);
}

void draw() {
  background(0);
  noFill();
  stroke(255);
  strokeWeight(2);

  int ll = 100;//int(map(mouseX, 0, width, 10, 500));
  int n_samples = 250;
  int n_effect = 16;//int(map(mouseY, 0, height, 1, 30));

  float s = ll/n_effect;

  int n_rot = int(map(mouseX, 0, width, -5, 5));
  int sp_rot = int(map(mouseY, 0, height, 0, 5));

  // typo.draw();

  

  TypoWaves test_effect = new TypoWaves(typo, s, 0, 0, 0);

  test_effect.update();
  // test_effect.drawExt();

  for (int i = 0; i < n_effect; i++){


    println(i, typo.getShape().children[0].countPaths(), typo.getShape().getPoints().length);
    Typo typo2 = new Typo(this, new RShape(test_effect.getOutShape()), new RPoint(width/2, height/2));

    test_effect = new TypoWaves(typo2, s + i*sp_rot, 0, 1 + n_rot * i, 2 + i);

    test_effect.update();
    stroke(255, map(i, 0, n_effect, 255, 20));
    test_effect.drawExt();
  }
}
