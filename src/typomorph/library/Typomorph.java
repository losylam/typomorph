/**
 * Copyright 2019 Laurent Malys
 *
 * This file is part of TypoMorph.
 *
 * TypoMorph is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * TypoMorph is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with TypoMorph.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

package typomorph;

import processing.core.*;
import geomerative.*;

/**
 * Typomorph main class
 *
 * Use to have a common interface to initialize Typo objects
 *
 * @author Laurent Malys
 * @author www.laurent-malys.fr
 */
public class Typomorph implements PConstants{

    static String default_font = "";
    static int default_text_size = 100;
    static int default_samples = 0;
    static int default_sampling_type = 0;

    /**
     * the parent PApplet
     */
    public static PApplet applet;

    /**
     * Version of the library
     */
    public final static String VERSION = "##library.prettyVersion##";

    /**
     * Uniform sampling: Try to get uniform length of segment across all shapes and paths
     */
    public final static int UNIFORM = 0;

    /**
     * Sampling by shape: Try to get the same number of segment for each shape
     * (Letters with longest contour will have longest segments)
     */
    public final static int BYSHAPE = 1;

    /**
     * Sampling by path: Try to get the same number of segment for each path
     */
    public final static int BYPATH  = 2;


    /**
     * Init typomorpoh library (to be called inside setup() function)
     *
     * @param parentApplet the parent PApplet
     */
    public Typomorph(PApplet parentApplet) {
        applet = parentApplet;

        welcome();
    }

    /**
     * Init typomorpoh library (to be called inside setup() function)
     *
     * @param parentApplet the parent PApplet
     */
    public static void init(PApplet parentApplet){
        applet = parentApplet;

        welcome();
    }


    /**
     * Set default font for Typo object initialised with Typomorph.createTypo()
     *
     * @param font name of the font file
     * @param text_size Text size in pixels
     */
    public static void setDefaultFont(String font, int text_size){
        default_font = font;
        default_text_size = text_size;
    }


    /**
     * Set default sampling type and value for Typo object initialised with Typomorph.createTypo()
     *
     * @param n_samples Number of samples
     * @param sampling_type Type of sampling
     */
    public static void setDefaultSampling(int n_samples, int sampling_type){
        default_samples = n_samples;
        default_sampling_type = sampling_type;
    }


    /**
     * Create a new Typo object
     *
     * @param txt Text
     * @param font Name of the font file
     * @param text_size Size of the font in pixels
     * @param pos Position of the shape on the canvas
     */
    public static Typo createTypo(String txt, String font, int text_size, RPoint pos){
        Typo typo = new Typo(applet, txt, font, text_size, pos);
        if (default_samples != 0){
            typo.resample(default_samples, default_sampling_type);
        }
        return typo;
    }


    /**
     * Create a new Typo object with default font and font size
     *
     * @param txt Text
     * @pos Position of the shape on the canvas as RPoint
     */
    public static Typo createTypo(String txt, RPoint pos){
        return createTypo(txt, default_font, default_text_size, pos);
    }


    /**
     * Create a new Typo object with default font and font size
     *
     * @param txt Text
     * @param pos_x X position of the shape on the canvas
     * @param pos_y Y position of the shape on the canvas
     */
    public static Typo createTypo(String txt, float pos_x, float pos_y){
        return createTypo(txt, default_font, default_text_size, new RPoint(pos_x, pos_y));
    }


    /**
     * Show basic structure of a shape
     *
     * @param shape_in Shape
     */
    public static void showStructure(RShape shape_in){
        PApplet.println("Structure off shape");
        PApplet.println("-------------------");
        int n_children = shape_in.countChildren();
        int tot_points = shape_in.getPoints().length;
        PApplet.println("");
        PApplet.println("num children: ", n_children);
        PApplet.println("tot points: ", tot_points);

        for (int i_c = 0; i_c < shape_in.countChildren(); i_c ++){
            RShape shape_loc = shape_in.children[i_c];
            int n_paths = shape_loc.countPaths();
            PApplet.println("|");
            PApplet.println("|-- shape [", i_c, "], n_paths= ", n_paths);
            for (int i_p = 0; i_p < shape_loc.countPaths(); i_p ++){
                RPath path_loc = shape_loc.paths[i_p];
                int n_points = path_loc.getPoints().length;
                PApplet.println("  |-- path [", i_p, "], n_points= ", n_points);
            }
        }
    }

    public static void showStructure(Typo typo_in){
        showStructure(typo_in.getShape());
    }

    /**
     * Print library infos
     */
    private static void welcome() {
        System.out.println("##library.name## ##library.prettyVersion## by ##author##");
    }


    /**
     * return the version of the Library.
     *
     * @return String
     */
    public static String version() {
        return VERSION;
    }
}

