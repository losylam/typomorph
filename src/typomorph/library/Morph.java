/**
 * Copyright 2019 Laurent Malys
 *
 * This file is part of TypoMorph.
 *
 * TypoMorph is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * TypoMorph is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with TypoMorph.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

package typomorph;

import java.util.*;
import processing.core.*;
import geomerative.*;



/**
 * Morph between shapes
 */
public class Morph {

    /**
     * The parent PApplet
     */
    public PApplet applet;

    Typo typoA;
    Typo typoB;
    Typo typoC;

    RPoint[] pointsA;
    RPoint[] pointsB;
    RPoint[] PointsC;

    RPoint[] points;
    RPoint[] points_lines;


    /**
     * Create empty Morph
     */
    public Morph(PApplet parentPApplet){
        applet = parentPApplet;
    }


    /**
     * Create a Morph object between two Typo
     *
     * @param typoA_in
     * @param typoB_in
     */
    public Morph(Typo typoA_in, Typo typoB_in){
        this(typoA_in.applet);

        typoA = typoA_in;
        typoB = typoB_in;

        pointsA = typoA.getShape().getPoints();
        pointsB = typoB.getShape().getPoints();
    }


    /**
     * Return points of the morphing shape at morph
     *
     * @param morph Morphing value between 0 and 1
     * @return List of RPoint of the morphed shape
     */
    public RPoint[] getPoints(float morph) { //Get list of points at morph (0.0<morph<1.0) 

        // Choose the minimum value if typoA and typoB dont have the same number of vertices
        int min_points = Math.min(pointsA.length, pointsB.length);

        RPoint[] pts_out = new RPoint[min_points];
        for (int i = 0; i < min_points; i++) {

            // The morphing is here
            float pos_x = PApplet.lerp(pointsA[i].x, pointsB[i].x, morph);
            float pos_y = PApplet.lerp(pointsA[i].y, pointsB[i].y, morph);
            pts_out[i] = new RPoint(pos_x, pos_y);
        }

        return pts_out;
    }

    public RShape morphByPath(float morph){
        RShape out = new RShape();
        RShape shapeA = typoA.getShape();
        RShape shapeB = typoB.getShape();
        int n_shapesA = shapeA.countChildren();
        int n_shapesB = shapeB.countChildren();

        int n_shapes = Math.min(n_shapesA, n_shapesB);

        for (int i_shape = 0; i_shape < n_shapes; i_shape++){
            RShape childA = shapeA.children[i_shape];
            RShape childB = shapeB.children[i_shape];
            int n_pathsA = childA.countPaths();
            int n_pathsB = childB.countPaths();
            int n_paths = Math.min(n_pathsA, n_pathsB);

            RShape child = new RShape();
            for (int i_path = 0; i_path < n_paths;i_path++){
                RPath pathA = childA.paths[i_path];
                RPath pathB = childB.paths[i_path];

                RPath path_out = morphPath(pathA, pathB, morph);
                path_out.addClose();

                child.addPath(path_out);
            }

            if (n_pathsA > n_pathsB){
                for (int i_path = n_paths; i_path<n_pathsA;i_path++){
                    RPath path_out = new RPath(childA.paths[i_path]);
                    path_out.scale(1-morph,path_out.getCenter());
                    child.addPath(path_out);
                }
            }else{
                for (int i_path = n_paths; i_path<n_pathsB;i_path++){
                    RPath path_out = new RPath(childB.paths[i_path]);
                    path_out.scale(morph, path_out.getCenter());
                    child.addPath(path_out);
                }
            }

            out.addChild(child);
        }

        if (n_shapesA > n_shapesB){
            for (int i_child = n_shapes; i_child<n_shapesA;i_child++){
                RShape shape_out = new RShape(shapeA.children[i_child]);
                shape_out.scale(1-morph,shape_out.getCenter());
                out.addChild(shape_out);
            }
        }else{
            for (int i_child = n_shapes; i_child<n_shapesB;i_child++){
                RShape shape_out = new RShape(shapeB.children[i_child]);
                shape_out.scale(morph, shape_out.getCenter());
                out.addChild(shape_out);
            }
        }

        return out;
    }

    public RPath morphPath(RPath p1, RPath p2, float morph){
        RPoint[] pts1 = reorderPoints(p1);
        RPoint[] pts2 = reorderPoints(p2);

        RPoint[] pts  = new RPoint[pts1.length];

        for (int i=0; i<pts.length;i++){
            float pos_x = PApplet.lerp(pts1[i].x, pts2[i].x, morph);
            float pos_y = PApplet.lerp(pts1[i].y, pts2[i].y, morph);
            pts[i] = new RPoint(pos_x, pos_y);
        }
        return new RPath(pts);
    }


    public RShape shapeMorph(float morph){
        RShape out = new RShape();
        for (int i_shape = 0; i_shape < typoA.getShape().countChildren(); i_shape++){
            RShape old_shape = typoA.getShape().children[i_shape];
            RShape old_shapeB = typoB.getShape().children[i_shape];
            RShape new_shape = new RShape(old_shape);
            for (int i_path = 0; i_path < old_shape.countPaths(); i_path++){
                RCommand[] ptsA = old_shape.paths[i_path].commands;
                RCommand[] ptsB = old_shapeB.paths[i_path].commands;
                RCommand[] pts = new_shape.paths[i_path].commands;

                for (int i_pt = 0; i_pt < pts.length; i_pt ++){
                    pts[i_pt].startPoint.x = PApplet.lerp(ptsA[i_pt].startPoint.x, ptsB[i_pt].startPoint.x, morph);
                    pts[i_pt].startPoint.y = PApplet.lerp(ptsA[i_pt].startPoint.y, ptsB[i_pt].startPoint.y, morph);

                    pts[i_pt].endPoint.x = PApplet.lerp(ptsA[i_pt].endPoint.x, ptsB[i_pt].endPoint.x, morph);
                    pts[i_pt].endPoint.y = PApplet.lerp(ptsA[i_pt].endPoint.y, ptsB[i_pt].endPoint.y, morph);

                    RPoint[] cpsA = ptsA[i_pt].controlPoints;
                    RPoint[] cpsB = ptsB[i_pt].controlPoints;
                    RPoint[] cps = pts[i_pt].controlPoints;

                    if (cps!=null){
                        for (int i_cp = 0; i_cp < cps.length; i_cp++){
                            cps[i_cp].x = PApplet.lerp(cpsA[i_cp].x, cpsB[i_cp].x, morph);
                            cps[i_cp].y = PApplet.lerp(cpsA[i_cp].y, cpsB[i_cp].y, morph);
                        }
                    }
                }
            }
            out.addChild(new_shape);
        }
        return out;
    }

    public RShape shapeMorphDouble(float morph, float morph2) {

        RShape out = new RShape();

        // for each child shapcp
        for (int i_shape = 0; i_shape < typoA.getShape().countChildren(); i_shape++) {

            RShape old_shape = typoA.getShape().children[i_shape];
            RShape old_shapeB = typoB.getShape().children[i_shape];
            RShape old_shapeC = typoC.getShape().children[i_shape];

            RShape new_shape = new RShape(old_shape);

            // for each path of the child shape
            for (int i_path = 0; i_path < old_shape.countPaths(); i_path++) {
                RCommand[] cmdA = old_shape.paths[i_path].commands;
                RCommand[] cmdB = old_shapeB.paths[i_path].commands;
                RCommand[] cmdC = old_shapeC.paths[i_path].commands;
                RCommand[] cmd = new_shape.paths[i_path].commands;

                // the morphing is here
                for (int i_pt = 0; i_pt < cmd.length; i_pt ++) {
                    cmd[i_pt].startPoint = lerpPoints(cmdA[i_pt].startPoint, 
                                                      cmdB[i_pt].startPoint, 
                                                      cmdC[i_pt].startPoint, 
                                                      morph, 
                                                      morph2);

                    cmd[i_pt].endPoint = lerpPoints(cmdA[i_pt].endPoint, 
                                                    cmdB[i_pt].endPoint, 
                                                    cmdC[i_pt].endPoint, 
                                                    morph, 
                                                    morph2);

                    RPoint[] cpsA = cmdA[i_pt].controlPoints;
                    RPoint[] cpsB = cmdB[i_pt].controlPoints;
                    RPoint[] cpsC = cmdC[i_pt].controlPoints;
                    RPoint[] cps = cmd[i_pt].controlPoints;

                    if (cps!=null){
                        for (int i_cp=0; i_cp < cmd[i_pt].controlPoints.length;i_cp++){
                            cps[i_cp] = lerpPoints(cpsA[i_cp], cpsB[i_cp], cpsC[i_cp], morph, morph2);
                        }
                    }
                }
            }

            out.addChild(new_shape);
        }

        return out;
    }


    /**
     * Return the list of points starting by the point to the top left corner
     *
     * @param p Input Path
     * @return List of reordered points
     */
    static RPoint[] reorderPoints(RPath p){
        RPoint pt = p.getTopLeft();
        RPoint[] pts = p.getPoints();
        float d = pt.dist(pts[0]);
        int num_min = 0;
        for (int i = 1; i < pts.length;i++){
            float dd = pt.dist(pts[i]);
            if (dd < d){
                d = dd;
                num_min = i;
            }
        }

        RPoint[] out = new RPoint[pts.length];
        for (int i=0 ; i < pts.length;i++){
            out[i] = pts[(num_min+i)%pts.length];
        }
        return out;
    }


    /**
     * Linear interpolation between 3 points 
     * @param pts1
     * @param pts2
     * @param pts3
     * @param m1 interpolation between pts1 and pts2
     * @param m2 interpolation between pts1 and pts3
     */
    static RPoint lerpPoints(RPoint pts1,RPoint pts2,RPoint pts3, float m1, float m2){
        float x = pts1.x + (pts2.x-pts1.x)*m1 + (pts3.x-pts1.x)*m2;
        float y = pts1.y + (pts2.y-pts1.y)*m1 + (pts3.y-pts1.y)*m2;

        return new RPoint(x, y);
    }


    /**
     * Draw the intermediate shape
     *
     * @param morph
     */
    public void draw(float morph) {
        RShape sh = morphByPath(morph);
        sh.draw();
    }


    /**
     * Draw the intermediate shape as points
     *
     * @param morph
     */
    public void drawPoints(float morph) {
        points = getPoints(morph);
        for (int i = 0; i < points.length; i++) {
            applet.point(points[i].x, points[i].y);
        }
    }


    /**
     * Draw lines between morph1 and morph2
     *
     * @param morph1
     * @param morph2
     */
    public void drawLines(float morph1, float morph2) {
        points = getPoints(morph1);
        points_lines = getPoints(morph2);

        for (int i = 0; i < points.length; i++) {
            applet.line(points[i].x, points[i].y, points_lines[i].x, points_lines[i].y);
        }
    }


    /**
     * Draw the shape at morph
     * only if the two original shape have the exact same topology
     *
     * @param morph
     */
    public void drawShape(float morph) {
        RShape morph_shape = shapeMorph(morph); 
        morph_shape.draw();
    }


    /**
     * Draw the shape at morph
     * only if the two original shape have the exact same topology
     *
     * @param morph1
     * @param morph2
     */
    public void drawShapeDouble(float morph1, float morph2) {
        RShape morph_shape = shapeMorphDouble(morph1, morph2); 
        morph_shape.draw();
    }
}
