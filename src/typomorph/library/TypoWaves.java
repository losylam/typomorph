package typomorph;

import processing.core.*;
import geomerative.*;


/**
 * A wave effect along contours of a shape
 *
 * Create inside and outside wave by rotating a segment around each
 * point on the paths
 *
 * @author Laurent Malys
 * @author www.laurent-malys.fr
 */
public class TypoWaves extends Typo{
    /**
     * Group of shapes containing the inside wave (direction 1)
     */
    public RShape wave_int;
    /**
     * Group of shapes containing the outside wave (direction 2)
     */
    public RShape wave_ext;

    float rotate_num = 1.0f;         // number of rotation accross the longest path
    float rotate_speed = 0.0f;       // rotation speed (rad/s)

    // length of the line in the two directions
    float amplitude_1 = 50.0f;
    float amplitude_2 = -5.0f;

    // mouse interactivity
    boolean interactive = false;     // on/off
    float interactive_radius = 0.0f; // radius of action
                                     // the ratio is interpolated between the center of the edge
    float interactive_close = 1.0f;    // ratio at the position the mouse
    float interactive_far = 0.0f;      // ratio when outside of radius

    boolean angle_auto = true;
    float angle = 0.0f;
    // int last_update;
    // float angle = 0.0;

    /**
     * Create TypoWaves with default parameters
     *
     * @param typo_in Typo input
     */
    public TypoWaves (Typo typo_in){
        super(typo_in);
        //    last_update = millis();
    }


    /**
     * Create TypoWaves
     *
     * @param typo_in Typo input
     * @param amplitude_1_in Length of the wave in the first direction
     * @param amplitude_2_in Length of the wave in the second direction
     */
    public TypoWaves (Typo typo_in, float amplitude_1_in, float amplitude_2_in){
        super(typo_in);
        //    last_update = millis();
        amplitude_1 = amplitude_1_in;
        amplitude_2 = amplitude_2_in;
    }


    /**
     * Create TypoWaves
     *
     * @param typo_in Typo input
     * @param amplitude_1_in Length of the wave in the first direction
     * @param amplitude_2_in Length of the wave in the second direction
     * @param rotate_num_in Number of rotation across perimeter
     * @param rotate_speed_in Angle velocity in rad/s
     */
    public TypoWaves (Typo typo_in, float amplitude_1_in, float amplitude_2_in, float rotate_num_in, float rotate_speed_in){
        super(typo_in);
        //    last_update = millis();
        amplitude_1 = amplitude_1_in;
        amplitude_2 = amplitude_2_in;
        rotate_num = rotate_num_in;
        rotate_speed = rotate_speed_in;
    }


    /**
     * Change wave parameters
     *
     * @param amplitude_1_in Length of the wave in the first direction
     * @param amplitude_2_in Length of the wave in the second direction
     * @param rotate_num_in Number of rotation across perimeter
     * @param rotate_speed_in Angle velocity in rad/s
     */
    public void setWaves(float amplitude_1_in, float amplitude_2_in, float rotate_num_in, float rotate_speed_in){
        amplitude_1 = amplitude_1_in;
        amplitude_2 = amplitude_2_in;
        rotate_num = rotate_num_in;
        rotate_speed = rotate_speed_in;
    }


    /**
     * Change wave rotation parameters
     *
     * @param rotate_num_in Number of rotation across perimeter
     * @param rotate_speed_in Angle velocity in rad/s
     */
    public void setWavesRot(float rotate_num_in, float rotate_speed_in){
        rotate_num = rotate_num_in;
        rotate_speed = rotate_speed_in;
    }


    /**
     * Change amplitures
     *
     * @param rotate_num_in Number of rotation across perimeter
     * @param rotate_speed_in Angle velocity in rad/s
     */
    public void setAmplitude(float amplitude_1_in, float amplitude_2_in){
        amplitude_1 = amplitude_1_in;
        amplitude_2 = amplitude_2_in;
    }


    /**
     * Set the wave length to be calculated according to mouse distance
     *
     * @param interactive_or_not Enable/disable interactivity
     * @param val_distance Max distance of the mouse
     * @param val_far ratio applied when the mouse is far
     * @param val_close ratio applied when the mouse is close
     */
    public void setInteractive(boolean interactive_or_not, float val_distance, float val_far, float val_close){
        interactive = interactive_or_not;
        interactive_radius = val_distance;
        interactive_close = val_close;
        interactive_far = val_far;
    }


    /**
     * Set angle (turn animation mode off)
     *
     * @param angle_in 
     */
    public void setAngle(float angle_in){
        angle_auto = false;
        angle = angle_in;
    }


    /**
     * Update wave effect
     */
    public void update(){

        float max_length = getMaxLength();

        wave_ext = new RShape();
        wave_int = new RShape();

        for (int i_child = 0; i_child < sampledShape.countChildren(); i_child++){
            waveShape(sampledShape.children[i_child], max_length);
        }
    }


    /**
     * Create a wave around the shape
     * Add the wave shapes as children of wave_int and wave_ext
     *
     * @param shape
     * @param max_length Length of the longest path of the shape
     */
    public void waveShape(RShape shape, float max_length){
        RShape[] shapes = new RShape[2];
        shapes[0] = new RShape();
        shapes[1] = new RShape();

        for (int i_p = 0; i_p < shape.countPaths(); i_p ++){
            RPath[] paths = new RPath[2];
            paths = wavePath(shape.paths[i_p], max_length);

            shapes[0].addPath(paths[0]);
            shapes[1].addPath(paths[1]);
        }
        wave_int.addChild(shapes[0]);
        wave_ext.addChild(shapes[1]);
    }


    /**
     * Create a wave around a path
     *
     * @param path
     * @param max_length Length of the longest path of the shape
     * @return An array containing the inside wave and the outside wave
     */
    public RPath[] wavePath(RPath path, float max_length){

        RPath[] paths = new RPath[2];

        float l = path.getCurveLength();

        // number of rotation depends of the ratio between the length of the path
        // and the longest contour
        double n_rot = Math.floor(max_length / l);

        // init new paths
        RPath path_int = new RPath();
        RPath path_ext = new RPath();

        RPoint[] points = path.getPoints();

        int n_points_loc = points.length;

        // animated angle
        float cur_angle = angle;
        if (angle_auto){
            cur_angle = applet.millis() * rotate_speed / 1000.0f;
        }

        for (int i = 0; i < points.length - 1; i++){
            RPoint pt = points[i];
            RPoint p1 = new RPoint(pt); // copy the point
            RPoint p2 = new RPoint(pt); // copy the point

            // init transform matrix
            RMatrix transform = new RMatrix();

            // handle rotation

            // angle that changes along the path
            float loc_angle = i*PApplet.TWO_PI*(int)(rotate_num/n_rot)/(n_points_loc-1);

            // rotate around point
            transform.rotate(cur_angle + loc_angle, pt);

            RMatrix t_p1 = new RMatrix(transform);
            RMatrix t_p2 = new RMatrix(transform);

            // handle translation
            float loc_amplitude_1 = amplitude_1;
            float loc_amplitude_2 = amplitude_2;

            // mouse interactive mode
            if (interactive){
                float d = pt.dist(new RPoint(applet.mouseX, applet.mouseY)); // distance between mouse and current point
                float ratio = PApplet.map(d, 0, interactive_radius, interactive_far, interactive_close);
                ratio = Math.min(Math.max(0, ratio), 1);
                loc_amplitude_1 *= ratio;
                loc_amplitude_2 *= ratio;
            }

            t_p1.translate(loc_amplitude_1, 0);
            t_p2.translate(loc_amplitude_2, 0);

            // apply transformation
            p1.transform(t_p1);
            p2.transform(t_p2);

            if (i == 0){
                // init paths with the first point
                path_ext = new RPath(p1);
                path_int = new RPath(p2);
            }else{
                // add point to paths
                path_ext.addLineTo(p1);
                path_int.addLineTo(p2);
            }
        }

        // close paths
        path_int.addClose();
        path_ext.addClose();

        paths[0] = path_int;
        paths[1] = path_ext;

        // return interior path and exterior path as a list
        return paths;
    }


    /**
     * Return the outsize wave
     *
     * @return A shape containing the outside wave
     */
    public RShape getOutShape(){
        return wave_ext;
    }


    /**
     * Draw the external wave
     */
    public void drawExt(){
        wave_ext.draw();
    }


    /**
     * Draw the internal wave
     */
    public void drawInt(){
        wave_int.draw();
    }


    /**
     * Draw lines between waves
     */
    public void drawLines(){
        RPoint[] pts1 = wave_ext.getPoints();
        RPoint[] pts2 = wave_int.getPoints();

        int tot_points = pts1.length;
        if (pts1.length != pts2.length){
            tot_points = Math.min(pts1.length, pts2.length);
            System.out.println("drawLines n_points count problem");
        }

        for (int i = 0; i < tot_points; i++){
            applet.line(pts1[i].x, pts1[i].y, pts2[i].x, pts2[i].y);
            // System.out.println(pts2[i].x);
        }
    }


    /**
     * Draw lines between contours on graphics
     *
     * @param pg PGraphics to draw on
     */
    public void drawOnGraphics(PGraphics pg){
        RPoint[] pts1 = wave_ext.getPoints();
        RPoint[] pts2 = wave_int.getPoints();

        int tot_points = pts1.length;
        if (pts1.length != pts2.length){
            tot_points = Math.min(pts1.length, pts2.length);
            System.out.println("drawLines n_points count problem");
        }

        for (int i = 0; i < tot_points; i++){
            pg.line(pts1[i].x, pts1[i].y, pts2[i].x, pts2[i].y);
            // System.out.println(pts2[i].x);
        }

    }


    /**
     * Draw inside and outside waves and lines between waves
     */
    public void draw(){
        draw(true, true, true);
    }


    /**
     * Draw TypoWaves
     *
     * @param draw_int Draw inside wave if true
     * @param draw_ext Draw outside wave if true
     * @param draw_lines Draw lines between waves if true
     */
    public void draw(boolean draw_int, boolean draw_ext, boolean draw_lines){
        if (draw_int){
            drawInt();
        }
        if (draw_ext){
            drawExt();
        }
        if (draw_lines){
            drawLines();
        }
    }


}
