/**
 * Copyright 2019 Laurent Malys
 *
 * This file is part of TypoMorph.
 *
 * TypoMorph is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * TypoMorph is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with TypoMorph.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

package typomorph;

import java.util.*;
import processing.core.*;
import geomerative.*;


/**
 * Morphing between two shapes with bezier curves created from wave effect
 */
public class MorphWaves extends Morph{

    TypoWaves typoA;
    TypoWaves typoB;
    int n_samples;


    /**
     * Create empty MorphWaves
     */
    public MorphWaves(PApplet parentPApplet){
        super(parentPApplet);
    }


    /**
     * Create MorphWaves between typoA_in and typoB_in
     *
     * @param typoA_in
     * @param typoB_in
     */
    public MorphWaves(TypoWaves typoA_in, TypoWaves typoB_in){
        this(typoA_in.applet);

        typoA = typoA_in;
        typoB = typoB_in;

        pointsA = typoA.getShape().getPoints();
        pointsB = typoB.getShape().getPoints();

        n_samples = Math.min(pointsA.length, pointsB.length);
    }


    /**
     * Draw bezier curves between TypoWaves
     *
     * @param morph_center Center of the curve (between 0 and 1 - ratio from the full curve)
     * @param morph_length Length of the bezier curve (between 0 and 1 - ratio from the full curve)
     */
     public void bezierMorph(float morph_center, float morph_length){
        typoA.update();
        typoB.update();

        RPoint[] ptsA_int = typoA.wave_int.getPoints();
        RPoint[] ptsA_ext = typoA.wave_ext.getPoints();
        RPoint[] ptsB_int = typoB.wave_int.getPoints();
        RPoint[] ptsB_ext = typoB.wave_ext.getPoints();

        float morph1 = Math.max(Math.min(morph_center+morph_length/2, 1), 0);
        float morph2 = Math.min(Math.max(morph_center-morph_length/2, 0), 1);

        for (int i = 0; i < n_samples; i++){
            RPath p = new RPath(ptsA_int[i]);
            p.addBezierTo(ptsA_ext[i], ptsB_ext[i], ptsB_int[i]);
            RPath p2 = p.split(morph1)[0];

            if (morph1 > 0.01 && morph1 > morph2){
                p2 = p2.split(morph2/morph1)[1];
            }

            p2.draw();
        }
    }
}

