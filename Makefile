PROCESSING_JAVA=/home/laurent/.local/apps/processing/processing-java

compile:
	ant -f resources/build.xml

doc:
	ant -f resources/typomorph-javadoc.xml

distribute: compile doc
	echo "distribute"

example:
	$(PROCESSING_JAVA) --sketch="test/hello_waves" --run
	diff test/hello_waves/out_ref.tif test/hello_waves/out.tif
	$(PROCESSING_JAVA) --sketch="test/hello_morph_waves" --run
	diff test/hello_morph_waves/out_ref.tif test/hello_morph_waves/out.tif
