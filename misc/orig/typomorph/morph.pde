class Morph {
  /*
   *  Handle morphing between two Typomorph shapes
   */

  Typomorph typoA;
  Typomorph typoB;
  Typomorph typoC;

  RPoint[] pointsA;
  RPoint[] pointsB;
  RPoint[] pointsC;

  RPoint[] points;
  RPoint[] points_lines;

  Morph (Typomorph typoA_in, Typomorph typoB_in) {
    typoA = typoA_in;
    typoB = typoB_in;

    pointsA = typoA.getShape().getPoints();
    pointsB = typoB.getShape().getPoints();
  }

  Morph (RShape shape1, RShape shape2) {
    typoA = new Typomorph(shape1, new RPoint(0, 0));
    typoB = new Typomorph(shape2, new RPoint(0, 0));

    pointsA = typoA.getShape().getPoints();
    pointsB = typoB.getShape().getPoints();
  }  

  Morph (RShape shape1, RShape shape2, RShape shape3) {
    typoA = new Typomorph(shape1, new RPoint(0, 0));
    typoB = new Typomorph(shape2, new RPoint(0, 0));
    typoC = new Typomorph(shape3, new RPoint(0, 0));

    pointsA = typoA.getShape().getPoints();
    pointsB = typoB.getShape().getPoints();
    pointsC = typoC.getShape().getPoints();
  }

  RPoint[] getPoints(float morph) { //Get list of points at morph (0.0<morph<1.0) 

    // Choose the minimum value if typoA and typoB dont have the same number of vertices
    int min_points = min(pointsA.length, pointsB.length);

    RPoint[] pts_out = new RPoint[min_points];
    for (int i = 0; i < min_points; i++) {

      // The morphing is here
      float pos_x = lerp(pointsA[i].x, pointsB[i].x, morph);
      float pos_y = lerp(pointsA[i].y, pointsB[i].y, morph);
      pts_out[i] = new RPoint(pos_x, pos_y);
    }

    return pts_out;
  }

  RShape morphByPath(float morph){
    RShape out = new RShape();
    RShape shapeA = typoA.getShape();
    RShape shapeB = typoB.getShape();
    int n_shapesA = shapeA.countChildren();
    int n_shapesB = shapeB.countChildren();
    
    int n_shapes = min(n_shapesA, n_shapesB);
    
    for (int i_shape = 0; i_shape < n_shapes; i_shape++){
      RShape childA = shapeA.children[i_shape];
      RShape childB = shapeB.children[i_shape];
      int n_pathsA = childA.countPaths();
      int n_pathsB = childB.countPaths();
      int n_paths = min(n_pathsA, n_pathsB);
      
      RShape child = new RShape();
      for (int i_path = 0; i_path < n_paths;i_path++){
        RPath pathA = childA.paths[i_path];
        RPath pathB = childB.paths[i_path];
        
        RPath path_out = morphPath(pathA, pathB, morph);
        path_out.addClose();

        child.addPath(path_out);
      }

      if (n_pathsA > n_pathsB){
        for (int i_path = n_paths; i_path<n_pathsA;i_path++){
          RPath path_out = new RPath(childA.paths[i_path]);
          path_out.scale(1-morph,path_out.getCenter());
          child.addPath(path_out);
        }
      }else{
        for (int i_path = n_paths; i_path<n_pathsB;i_path++){
          RPath path_out = new RPath(childB.paths[i_path]);
          path_out.scale(morph, path_out.getCenter());
          child.addPath(path_out);
        }
      }

      out.addChild(child);
    }
    
    if (n_shapesA > n_shapesB){
      for (int i_child = n_shapes; i_child<n_shapesA;i_child++){
        RShape shape_out = new RShape(shapeA.children[i_child]);
        shape_out.scale(1-morph,shape_out.getCenter());
        out.addChild(shape_out);
      }
    }else{
      for (int i_child = n_shapes; i_child<n_shapesB;i_child++){
        RShape shape_out = new RShape(shapeB.children[i_child]);
        shape_out.scale(morph, shape_out.getCenter());
        out.addChild(shape_out);
      }
    }

    
    return out;
  }

  RPath morphPath(RPath p1, RPath p2, float morph){
    RPoint[] pts1 = reorderPoints(p1);
    RPoint[] pts2 = reorderPoints(p2);

    RPoint[] pts  = new RPoint[pts1.length];  

    for (int i=0; i<pts.length;i++){
      float pos_x = lerp(pts1[i].x, pts2[i].x, morph);
      float pos_y = lerp(pts1[i].y, pts2[i].y, morph);
      pts[i] = new RPoint(pos_x, pos_y);
    } 
    return new RPath(pts);
  }

  RPoint[] reorderPoints(RPath p){
    /*
     * Get the list of points starting by the point to the top left corner
     */
    RPoint pt = p.getTopLeft();
    RPoint[] pts = p.getPoints();
    float d = pt.dist(pts[0]);
    int num_min = 0;
    for (int i = 1; i < pts.length;i++){
      float dd = pt.dist(pts[i]);
      if (dd < d){
        d = dd;
        num_min = i;
      }
    }

    RPoint[] out = new RPoint[pts.length];
    for (int i=0 ; i < pts.length;i++){
      out[i] = pts[(num_min+i)%pts.length];
    }
    return out;
  }
  
  RShape shapeMorph(float morph){
    RShape out = new RShape();
    for (int i_shape = 0; i_shape < typoA.getShape().countChildren(); i_shape++){
      RShape old_shape = typoA.getShape().children[i_shape];
      RShape old_shapeB = typoB.getShape().children[i_shape];
      RShape new_shape = new RShape(old_shape);
      for (int i_path = 0; i_path < old_shape.countPaths(); i_path++){
        RCommand[] ptsA = old_shape.paths[i_path].commands;
        RCommand[] ptsB = old_shapeB.paths[i_path].commands;
        RCommand[] pts = new_shape.paths[i_path].commands;
        
        for (int i_pt = 0; i_pt < pts.length; i_pt ++){
          pts[i_pt].startPoint.x = lerp(ptsA[i_pt].startPoint.x, ptsB[i_pt].startPoint.x, morph); 
          pts[i_pt].startPoint.y = lerp(ptsA[i_pt].startPoint.y, ptsB[i_pt].startPoint.y, morph);
          
          pts[i_pt].endPoint.x = lerp(ptsA[i_pt].endPoint.x, ptsB[i_pt].endPoint.x, morph); 
          pts[i_pt].endPoint.y = lerp(ptsA[i_pt].endPoint.y, ptsB[i_pt].endPoint.y, morph);
        
          RPoint[] cpsA = ptsA[i_pt].controlPoints;
          RPoint[] cpsB = ptsB[i_pt].controlPoints;
          RPoint[] cps = pts[i_pt].controlPoints;
          
          if (cps!=null){
            for (int i_cp = 0; i_cp < cps.length; i_cp++){
              cps[i_cp].x = lerp(cpsA[i_cp].x, cpsB[i_cp].x, morph); 
              cps[i_cp].y = lerp(cpsA[i_cp].y, cpsB[i_cp].y, morph); 
            }
          }
        }
      }
      out.addChild(new_shape);
    }
    return out;
  }

  RShape shapeMorphDouble(float morph, float morph2) {

    RShape out = new RShape();

    // for each child shapcp
    for (int i_shape = 0; i_shape < typoA.getShape().countChildren(); i_shape++) {

      RShape old_shape = typoA.getShape().children[i_shape];
      RShape old_shapeB = typoB.getShape().children[i_shape];
      RShape old_shapeC = typoC.getShape().children[i_shape];

      RShape new_shape = new RShape(old_shape);

      // for each path of the child shape
      for (int i_path = 0; i_path < old_shape.countPaths(); i_path++) {
        RCommand[] cmdA = old_shape.paths[i_path].commands;
        RCommand[] cmdB = old_shapeB.paths[i_path].commands;
        RCommand[] cmdC = old_shapeC.paths[i_path].commands;
        RCommand[] cmd = new_shape.paths[i_path].commands;

        // the morphing is here
        for (int i_pt = 0; i_pt < cmd.length; i_pt ++) {
          cmd[i_pt].startPoint = lerpPoints(cmdA[i_pt].startPoint, 
                                            cmdB[i_pt].startPoint, 
                                            cmdC[i_pt].startPoint, 
                                            morph, 
                                            morph2);

          cmd[i_pt].endPoint = lerpPoints(cmdA[i_pt].endPoint, 
                                          cmdB[i_pt].endPoint, 
                                          cmdC[i_pt].endPoint, 
                                          morph, 
                                          morph2);
          
          RPoint[] cpsA = cmdA[i_pt].controlPoints;
          RPoint[] cpsB = cmdB[i_pt].controlPoints;
          RPoint[] cpsC = cmdC[i_pt].controlPoints;
          RPoint[] cps = cmd[i_pt].controlPoints;
          
          if (cps!=null){
            for (int i_cp=0; i_cp < cmd[i_pt].controlPoints.length;i_cp++){
              cps[i_cp] = lerpPoints(cpsA[i_cp], cpsB[i_cp], cpsC[i_cp], morph, morph2);
            }
          }
        }
      }

      out.addChild(new_shape);
    }

    return out;
  }

  RPoint lerpPoints(RPoint pts1,RPoint pts2,RPoint pts3, float m1, float m2){
    /*
     * Linear interpolation between 3 points 
     */
    float x = pts1.x + (pts2.x-pts1.x)*m1 + (pts3.x-pts1.x)*m2;
    float y = pts1.y + (pts2.y-pts1.y)*m1 + (pts3.y-pts1.y)*m2;

    return new RPoint(x, y);
  }
  //  -- END OF RDOUBLE --


  void drawPoints(float morph) {
    /*
     * Draw the intermediate shape as points
     */
    points = getPoints(morph);
    for (int i = 0; i < points.length; i++) {
      point(points[i].x, points[i].y);
    }
  }

  void drawLines(float morph1, float morph2) {
    /*
     * Draw the line between morph1 and morph2
     */
    points = getPoints(morph1);
    points_lines = getPoints(morph2);

    for (int i = 0; i < points.length; i++) {
      line(points[i].x, points[i].y, points_lines[i].x, points_lines[i].y);
    }
  }

  void drawShapeDouble(float morph, float morph2) {
    /*
     * Draw the shape at morph 
     * only if the two original shape have the exact same topology
     */
    RShape morph_shape = shapeMorphDouble(morph, morph2); 
    morph_shape.draw();
  }

  void drawShape(float morph) {
    /*
     * Draw the shape at morph 
     * only if the two original shape have the exact same topology
     */
    RShape morph_shape = shapeMorph(morph); 
    morph_shape.draw();
  }
}
