class EffectSpiral extends Effect{
  
  ArrayList<ArrayList<PVector>> pts;
  RShape cur_shape;
  int n_paths = 0;
  int pos = 0;
  int samples = 0;
  float vel = 5;

  
  EffectSpiral (Typomorph typo_in){
    super(typo_in);
    pts = new ArrayList<ArrayList<PVector>>();
    updateTypo(typo_in);
  }
  
  void updateTypo(Typomorph typo_in){
    cur_shape = typo_in.getShape();
    samples = typo_in.samples;
    int n_path_new = 0;
    for (int i_c = 0; i_c < cur_shape.countChildren(); i_c ++){
      n_path_new += cur_shape.children[i_c].countPaths();
    }
    
    if (n_path_new > pts.size()){
      for (int i = 0; i < n_path_new-pts.size(); i++){
        pts.add(new ArrayList<PVector>());
      }
    }
  }

  void update(float vel_in){
    vel = vel_in;
    update();
  }
  
  void update(){
    int cur_p = 0;
    int n_new_pts = 0;

    for (int i_path=0; i_path<pts.size(); i_path++){
      for (int i_pt=0; i_pt<pts.get(i_path).size(); i_pt++){
        if (pts.get(i_path).get(i_pt).z < - 500){
          pts.get(i_path).remove(i_pt);
        }else{
          pts.get(i_path).get(i_pt).z -= vel;
           }
      }
    }
    
    for (int i_c = 0; i_c < cur_shape.countChildren(); i_c ++){
      for (int i_p = 0; i_p < cur_shape.children[i_c].countPaths(); i_p ++){      
        RPoint[] path_pts = cur_shape.children[i_c].paths[i_p].getPoints();
        int loc_pos = pos;
        n_new_pts = int(path_pts.length);
        for (int i=0; i<n_new_pts; i++){
          RPoint p = path_pts[loc_pos%path_pts.length];
          pts.get(cur_p).add(new PVector(p.x, p.y, map(i, 0, n_new_pts, -vel, 0)));
          loc_pos ++;
        }
        cur_p ++;
      }
    }
    pos += n_new_pts;
  }
  
  void draw(){
    hint(DISABLE_DEPTH_TEST);
    for (int i_path=0; i_path<pts.size(); i_path++){
      beginShape();
      for (int i_pt=0; i_pt<pts.get(i_path).size(); i_pt++){

        
        PVector p = pts.get(i_path).get(i_pt);
        // if (pts.get(i_path).size()-i_pt < n_samples){
        //   stroke(255);
        //   strokeWeight(3);
        // }else{
          strokeWeight(3);
          //stroke(150);
          //          stroke(map(i_pt, 0, pts.get(i_path).size(), 0, 150));
          stroke(255, map(p.z, 0, -500, 255, 0));
          

          //        }
        
        
        vertex(p.x, p.y, p.z);
      }
      endShape();
      
      // int diff = pts.get(i_path).size() - 30000;
      // if (diff > 0){
      //   for (int i = 0; i < diff; i++){
      //     pts.get(i_path).remove(0);
      //   }
      // }

    }
  }
}
