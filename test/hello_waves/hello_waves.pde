import java.util.*;
import typomorph.*; 
import geomerative.*;

void setup() {
  size(1000, 1000, FX2D);

  RG.setPolygonizer(RG.UNIFORMSTEP);
  RG.init(this);

  Typomorph.init(this);
  Typomorph.setDefaultFont("FreeSans.ttf", 800);
}

void draw() {
  background(255);

  int n_samples = 200; 
  int n_rot = 5;
  float wave_size = 50;

  Typo typo = Typomorph.createTypo("A", new RPoint(height/2, width/2));
  typo.resample(n_samples, Typomorph.UNIFORM);

  TypoWaves waves = new TypoWaves(typo, wave_size, 0, n_rot, 0);

  waves.update();
  waves.drawLines();
  save("out.tif");
  noLoop();
  exit();
}
