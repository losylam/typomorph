import java.util.*;
import typomorph.*; 
import geomerative.*;

void setup() {
  size(1000, 1000, FX2D);

  RG.setPolygonizer(RG.UNIFORMSTEP);
  RG.init(this);

  Typomorph.init(this);
  Typomorph.setDefaultFont("FreeSans.ttf", 800);
}

void draw() {
  background(255);

  int n_samples = 500;
  int n_rot = int(map(mouseY, 0, height, 0, 10));
  float wave_size = 50;

  Typo typo = Typomorph.createTypo("A", new RPoint(height/2, width/2));
  typo.resample(n_samples, Typomorph.UNIFORM);

  TypoWaves waves = new TypoWaves(typo, wave_size, 0, n_rot, 1);

  //  waves.setAngle(map(mouseX, 0, width, 0, TWO_PI));
  waves.update();
  waves.drawLines();
}
