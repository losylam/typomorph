import java.util.*;
import typomorph.*; 
import geomerative.*;

Typomorph TM;

void setup() {
  size(1000, 1000, FX2D);
  smooth();

  RG.setPolygonizer(RG.UNIFORMSTEP);
  RG.setPolygonizerStep(1);
  RG.init(this);

  TM = new Typomorph(this);
  TM.setDefaultFont("FreeSans.ttf", 800);
}

void draw() {
  background(0);
  noFill();
  stroke(255);
  strokeWeight(2);

  int n_samples = int(map(mouseX, 0, width, 10, 500));
  int n_effect = int(map(mouseY, 0, height, 1, 5));

  float s = 180/n_effect;

  Typo typo = TM.createTypo("E", new RPoint(height/2, width/2));
  typo.resample(n_samples, TM.UNIFORM);

  TypoWaves test_effect = new TypoWaves(typo, s, 1, 2, 1);

  test_effect.update();
  test_effect.draw();

  for (int i = 0; i < n_effect; i++){

    stroke(255, map(i, 0, n_effect, 255, 10));

    typo = new Typo(this, test_effect.getOutShape());

    test_effect = new TypoWaves(typo, s, 1, 2 + i, 2 + i);

    test_effect.update();
    // test_effect.drawLines();
    test_effect.draw();
  }
}
