
import java.util.*;
import typomorph.*;
import geomerative.*;

void setup() {
  size(1000, 1000, FX2D);
  smooth();

  RG.setPolygonizer(RG.UNIFORMSTEP);
  RG.init(this);

  Typomorph.init(this);
  Typomorph.setDefaultFont("FreeSans.ttf", 800);
  Typomorph.setDefaultSampling(500, Typomorph.BYPATH);
}

void draw(){
  background(0);
  noFill();
  stroke(255);

  Typo typo1 = Typomorph.createTypo("A", width/2, height/2);
  Typo typo2 = Typomorph.createTypo("B", width/2, height/2);

  Morph morph = new Morph(typo1, typo2);

  float pos = float(mouseX)/width;

  morph.draw(pos);
}

