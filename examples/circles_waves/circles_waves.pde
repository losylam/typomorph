import typomorph.*;
import geomerative.*;


class CirclesWaves extends TypoWaves{
  public CirclesWaves(Typo typo_in, float amp1, float amp2, float rot, float rot_speed){
    super(typo_in, amp1, amp2, rot, rot_speed);
  }

  public void drawBezierCircle(float radius_1, float radius_2, RPoint center, float speed){
    RPoint[] pts_ext = wave_ext.getPoints();
    RPoint[] pts_int = wave_int.getPoints();

    float step = TWO_PI/pts_ext.length;

    float angle_start = speed * millis() / 1000.0f;

    for (int i = 0; i < pts_ext.length; i++){
      RPath n = new RPath(new RPoint(pts_int[i]));

      RPoint p1 = new RPoint(center.x + radius_1 * cos(-angle_start*1/3 + step*i - 1),
                             center.y + radius_1 * sin(-angle_start*1/3 + step*i - 1));

      RPoint p2 = new RPoint(p1.x + radius_2 * cos(angle_start + step*i*3),
                             p1.y + radius_2 * sin(angle_start + step*i*3));

      n.addBezierTo(pts_ext[i], p2, p1);
      n.draw();
    }
  }
}

CirclesWaves waves;

void setup(){
  size(1000, 1000, FX2D);
  noFill();
  smooth();

  RG.setPolygonizer(RG.UNIFORMSTEP);
  RG.setPolygonizerStep(1);
  RG.init(this);

  Typomorph.init(this);
  Typomorph.setDefaultFont("FreeSans.ttf", 400);
  Typomorph.setDefaultSampling(150, Typomorph.UNIFORM);

  Typo typo = Typomorph.createTypo("Y", new RPoint(width/2, height/2));
  waves = new CirclesWaves(typo, 300, 0, 1, 2);
}

void draw(){
  background(0);
  stroke(255, 150);
  strokeWeight(2);

  waves.update();
  waves.drawBezierCircle(400, 200, new RPoint(width/2, height/2), 0.5);
}
