import java.util.*;
import typomorph.*;
import geomerative.*;

MorphWaves morph_waves;

void setup() {
  size(1000, 1000, FX2D);
  smooth();

  RG.setPolygonizer(RG.UNIFORMSTEP);
  RG.init(this);

  Typomorph.init(this);
  Typomorph.setDefaultFont("FreeSans.ttf", 900);
  Typomorph.setDefaultSampling(300, Typomorph.UNIFORM);

  Typo typo1 = Typomorph.createTypo("A", width/2, height/2);
  Typo typo2 = Typomorph.createTypo("B", width/2, height/2);

  TypoWaves waves1 = new TypoWaves(typo1, 100, 0, 2, 1);
  TypoWaves waves2 = new TypoWaves(typo2, 100, 0, 2, 1);

  morph_waves = new MorphWaves(waves1, waves2);
}

void draw(){
  background(0);
  noFill();
  stroke(255);

  float pos = float(mouseX)/width;
  float w = map(abs(mouseX - width/2), 0, width/2, 0.5, 0.1);

  morph_waves.bezierMorph(pos, w);
}

