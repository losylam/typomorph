import java.util.*;
import typomorph.*;
import geomerative.*;

MorphWaves morph_waves;

String[] letters = {"A", "B", "C", "D", "E"};
int cpt = 0;
int n_frames = 30;

boolean record = false;

ArrayList<TypoWaves> waves;
int cur_wave = 0;

float ease(float p, float g) {
  if (p < 0.5)
    return 0.5 * pow(2*p, g);
  else
    return 1 - 0.5 * pow(2*(1 - p), g);
}

void setup() {
  size(1000, 1000, FX2D);
  smooth();

  RG.setPolygonizer(RG.UNIFORMSTEP);
  RG.init(this);

  Typomorph.init(this);
  Typomorph.setDefaultFont("FreeSans.ttf", 900);
  Typomorph.setDefaultSampling(250, Typomorph.UNIFORM);

  waves = new ArrayList<TypoWaves>();

  for (int i = 0; i < letters.length; i++){
    Typo typo = Typomorph.createTypo(letters[i], width/2, height/2);
    waves.add(new TypoWaves(typo, 100, 0, 2, 1));
  }
  morph_waves = new MorphWaves(waves.get(0), waves.get(1));
}

void draw(){
  background(0);
  noFill();
  stroke(255);
  strokeWeight(2);

  int i_frame = cpt%n_frames;

  float pos = ease(float(i_frame)/n_frames, 2);
  float w = map(abs(pos - 0.5), 0, 0.5, 0.5, 0.1);

  morph_waves.bezierMorph(pos, w);
  cpt +=1;

  if (cpt % n_frames == 0){
    cur_wave = (cur_wave + 1) % letters.length;
    morph_waves = new MorphWaves(waves.get(cur_wave), waves.get((cur_wave+1)%letters.length));
  }

  if (record){
    save("screen-" + nf(cpt, 4));
  }

  if (cpt >= n_frames * letters.length * 2){
    record = false;
  }
}

void keyPressed(){
  if (key == 'R'){
    record = true;
    cpt = 0;
  }
}
